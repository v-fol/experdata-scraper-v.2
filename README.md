# Experdata Scraper v.2 (task - 1)
## Usage

In the project directory, you can run:

### `pip install -r requirements.txt` - to install packages
### `python linkedin_scraper.py [id]` - to get json file with experience
<pre>
Example ids:
_NBVaXixjtPITx89qmr8dA
VLLzjWPS-ZoUMR0YfFh7Zw
JNOqVaV8LGFIBTon7M3ogg
</pre>

[ID in URL](https://i.ibb.co/YXwNZJ9/fff.jpg)

import argparse
import requests
import json
from itertools import islice
from bs4 import BeautifulSoup
from pprint import pprint


class LinkedinScraper():
    """
    A class used to get response on a Linkedin profile from search.google.com_/SearchConsoleUi/data/batchexecute

    ...

    Attributes
    ----------
    id : str
        id that search.google.com had generated once for a Linkedin profile

    Methods
    -------
    scrape()
        Prints the response status and returns the responce
    """

    def __init__(self, id):
        self.headers = {
            'authority': 'search.google.com',
            'sec-ch-ua': '"Chromium";v="94", "Google Chrome";v="94", ";Not A Brand";v="99"',
            'x-same-domain': '1',
            'content-type': 'application/x-www-form-urlencoded;charset=UTF-8',
            'sec-ch-ua-mobile': '?0',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.61 Safari/537.36',
            'sec-ch-ua-platform': '"Windows"',
            'accept': '*/*',
            'origin': 'https://search.google.com',
            'x-client-data': 'CI+2yQEIo7bJAQjBtskBCKmdygEItOnKAQjq8ssBCO/yywEInvnLAQjYhMwBCNyEzAEI5oTMAQi1hcwBCNyFzAEI/4XMAQiBhswBGKqpygEYjp7LARiG/csB',
            'sec-fetch-site': 'same-origin',
            'sec-fetch-mode': 'cors',
            'sec-fetch-dest': 'empty',
            'referer': 'https://search.google.com/',
            'accept-language': 'en-US,en;q=0.9',
            'cookie': 'OGPC=19025836-2:; OTZ=6176614_44_48_123900_44_436380; HSID=AUHGoy1pQ0TLr-6Ro; SSID=AxH5Y1JAP0cF7fliK; APISID=e3gbiG95640KcyjX/AcwCsBYFdqZndSRsE; SAPISID=D0v_Qtyr7DvMsLZC/AMrGN9h0Pjvfg5IjW; __Secure-1PAPISID=D0v_Qtyr7DvMsLZC/AMrGN9h0Pjvfg5IjW; __Secure-3PAPISID=D0v_Qtyr7DvMsLZC/AMrGN9h0Pjvfg5IjW; SID=CAiXIo5BLOU51q-5gPPe8IMXpJOxvImbNOBLfhtmK_-DCgLewhFkzrzdUp6D6WkfiI9dew.; __Secure-1PSID=CAiXIo5BLOU51q-5gPPe8IMXpJOxvImbNOBLfhtmK_-DCgLewXUKzZESRtWta9T0G6W9iA.; __Secure-3PSID=CAiXIo5BLOU51q-5gPPe8IMXpJOxvImbNOBLfhtmK_-DCgLelXpkl2J9-i6nBxhYWi-BHw.; SEARCH_SAMESITE=CgQI2ZMB; 1P_JAR=2021-10-05-12; NID=511=aoAfc9xwy2AZt94xtw57kRWXQXQS94rj3NG9qP4qqeEhaKlDqtFZPAWw9Xx6mNOrUO4wdV2vQFPFDi2-j6tKT_HgGmhOB-bABvamR8-aPjR47gvJUp73-2S8qPbAcBKdOYLQS42NB8B7jZhfFmRS4OaJ0wp_M8ZXTsiEb0IsrQqtGf5jjfJQJf4ni60oJZE2wf6I9NErbDesQSWHMLo2vFFhWw4xGr6j9ymmxMtD7ElzaneL6Z6rO2A7UjB1M6E0q2CMj-TYzCmXIWs1sXv9rzBpURjHplBqb8CUGBNipRZQ0rr22oXROCTwqfCUjnJCVEIAHo6UegNcbGlYw5Lo6OG0Jd5N2w3uHnxVoCYAbDIdkE4rc_Wop3BCtFE2kKg7jJ9jYv-0WU6cMOkkPB0ebmpmzjXAC_di4rj87Sv0qztDx_xrEYkaDMfgJv0yTFA; SIDCC=AJi4QfGhDsMh8qJH7Y1GpF0YbwYhIAgZlgHEwPhA3MhXzxGwpxUqTx0ROTycDOtugYEILPZfzoY; __Secure-3PSIDCC=AJi4QfEpicfY2SKtdWW-YAVc-hqbE9SymiyDXgYe_7WMh2RGPrkt_kxxA3qSn0VUVCnx1dyAr_Q',
        }
        self.params = (
            ('rpcids', 'OFNzWe,jx7UIe,Ce7rhb,C4lTm,ueowNe,zrEBoe,YDPhmb'),
            ('f.sid', '-6312198925598448696'),
            ('bl', 'boq_searchconsoleserver_20210929.03_p0'),
            ('hl', 'en'),
            ('_reqid', '1150732'),
            ('rt', 'c'),
        )
        self.id = id

    def scrape(self):
        data = {
            'f.req': f'[[["OFNzWe","[\\"{self.id}\\",1]",null,"3"],["jx7UIe","[\\"{self.id}\\"]",null,"4"],["Ce7rhb","[\\"{self.id}\\",1]",null,"6"],["C4lTm","[\\"{self.id}\\",1]",null,"10"],["ueowNe","[\\"{self.id}\\",1]",null,"12"],["zrEBoe","[\\"{self.id}\\"]",null,"14"],["YDPhmb","[\\"{self.id}\\",1]",null,"15"]]]',
            'at': 'ABvcoAetzDZd4j7s--cth8Tj0dPh:1633431798299'
        }

        self.response = requests.post(
            'https://search.google.com/_/SearchConsoleUi/data/batchexecute', headers=self.headers, params=self.params, data=data)

        print(f'Status code - {self.response.status_code}\n')

        self.response.close()
        return self.response


class Decoder(LinkedinScraper):
    """
    A class used to decode response from search.google.com_/SearchConsoleUi/data/batchexecute

    ...

    Attributes
    ----------
    id : str
        id that search.google.com had generated once for a Linkedin profile

    Methods
    -------
    decode()
        Decodes the response from search.google.com and returns HTML string
    """

    def __init__(self, id):
        super().__init__(id)
        self.response_html = ''

    def decode(self):
        response_body = self.scrape().content.decode('utf_8')
        # Looking for HTML part of the response, iterating through lines to find DOCTYPE inside
        for line in islice(response_body.splitlines(), 0, 20, 1):
            if 'DOCTYPE' in line:
                self.response_html = line

        unicode_char_list = [r'\u003c', r'\u003e', r'\n', r"\u003d\\", '\\']
        utf_char_list = ['<', '>', '\n', '=', '']
        # Converting unicode characters and \n to utf
        for unicode_char, utf_char in zip(unicode_char_list, utf_char_list):
            self.response_html = self.response_html.replace(
                unicode_char, utf_char)

        if not self.response_html:
            raise Exception('HTML is not found in response body')
        else:
            return self.response_html


class LinkedinParser(Decoder):
    """
    A class used to parse HTML response from search.google.com_/SearchConsoleUi/data/batchexecute

    ...

    Attributes
    ----------
    id : str
        id that search.google.com had generated once for a Linkedin profile

    Methods
    -------
    parse()
        Parses the HTML from search.google.com, pprints dict with the name nad experience of the person from the profile,
        creates experience.json incide root didertory with this information
    """

    def __init__(self, id):
        super().__init__(id)
        self.count = 0

    def parse(self):
        soup = BeautifulSoup(self.decode(), 'html.parser')
        # Parsing data from html
        name = soup.find('h1')
        experience_ul = soup.find('ul', class_="experience__list")
        try:
            experience_li_list = experience_ul.find_all('li')
        except AttributeError:
            experience_li_list = False

        def parsing_ul(li, type):
            if type == 'description':
                try:
                    response = li.find(
                        'p', class_="show-more-less-text__text--less").get_text().replace('\n', '').strip()
                    return response
                except AttributeError:
                    return None
            elif type == 'position':
                try:
                    response = li.find(
                        'h3', class_='profile-section-card__title').get_text().replace('\n', '').strip()
                    return response
                except AttributeError:
                    return None
            elif type == 'company':
                try:
                    response = li.find(
                        'a', class_='profile-section-card__subtitle-link').get_text().replace('\n', '').strip()
                    return response
                except AttributeError:
                    return None

        experience = {}
        experience['name'] = name.get_text().strip()

        if not experience_ul:
            experience['experience_presence'] = False
        else:
            experience['experience_presence'] = True

        # Creating dictionary with person experience
        if experience_li_list:
            for li in experience_li_list:
                experience[self.count] = {'position': parsing_ul(li, 'position'), 'description': parsing_ul(
                    li, 'description'), 'company': parsing_ul(li, 'company')}
                self.count += 1

        pprint(experience, sort_dicts=False)

        with open('experience.json', 'w') as file:
            json.dump(experience, file)


if __name__ == "__main__":
    args_parser = argparse.ArgumentParser(
        description='Get experience from linkedin profile')

    args_parser.add_argument('id',
                             metavar='id',
                             type=str,
                             help='id (22 characters) of the request from search.google.com/test/mobile-friendly')

    args = args_parser.parse_args()

    if len(args.id) == 22:
        parser = LinkedinParser(id=args.id)
        parser.parse()
    else:
        raise Exception('id should contain 22 characters')
